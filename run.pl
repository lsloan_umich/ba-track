#!/usr/bin/env perl

use strict;
use open ':std', ':encoding(UTF-8)';
use utf8;
use Data::Dumper;
use Data::Faker::Name;
use Digest::MD5 qw(md5_hex);
use Text::CSV;
use DBI;
use Scalar::Util qw(looks_like_number);
use Text::CSV;
use YAML;
use YAML::XS 'LoadFile';

## Define some global variables – could probably move
## these to a config file.
my $d_root = '';

## Our config file.
my $f_conf = $d_root . '/config.yaml';
my $config = LoadFile($f_conf);

## The file from which we'll read te raw events from the UDP
## Event store and the file to which we'll output enriched
## events with metadata culled from the UDP Context store.
my $f_input_events   = $d_root . '/input/launch_events.csv';
my $f_output_events  = $d_root . '/output/raw_events.csv';
my $f_output_person = $d_root . '/output/raw_person.csv';
my $f_output_course  = $d_root . '/output/raw_course.csv';
my $f_output_person_course = $d_root . '/output/raw_person_course.csv';

## For fake names. A faker module, and a has to
## track fame names by Canvas identifier.
my $faker = Data::Faker::Name->new();
my $fake_names = {};

## Load all of the person and course offering data from the  Context store.
## Since it is not yet in the Context store, load Instructure's tools data
## from the UDW.
##
my $cs_persons           = &load_cs_persons($config);
my $cs_course_offerings  = &load_cs_course_offerings($config);
#my $udw_persons          = &load_udw_persons($config);
my $udw_course_offerings = &load_udw_course_offerings($config);
my $udw_tools            = &load_udw_tools($config);

## We pass this function the path to the launch events file,
## which we generate from a UDP Event store.
##
## That becomes the basis for establishing every tool launch in
## every course in the LMS.
##
## Note that this will all be LMS-sourced data, so we will have
## a mix of SIS-native and LMS-native courses reprenseted in the
## data.
##
my $events_output =
  &process_launch_events(
    $f_input_events,
    $f_output_events,
    $cs_persons,
    $cs_course_offerings,
#    $udw_persons,
    $udw_course_offerings,
    $udw_tools
);

## After processing the events, we set a few feature
## data sets for our dimensions.
&load_and_save_person_stats($config, $f_output_person);
&load_and_save_course_stats($config, $f_output_course, $udw_course_offerings);
&load_and_save_person_course_stats($config, $f_output_person_course);

exit;

################################################################################

sub process_launch_events($$$$$$$) {
  my(
    $f_input_events,
    $f_output_events,
    $cs_persons,
    $cs_course_offerings,
#    $udw_persons,
    $udw_course_offerings,
    $udw_tools
  ) = @_;

  ## Track the count of events that we process.
  my $count_events           = 0;
  my $count_match_cs_course  = 0;
  my $count_match_udw_course = 0;
  my $count_match_no_course  = 0;
  my $count_match_cs_person  = 0;
  my $count_tools            = 0;
  my $count_links            = 0;
  my $count_unknown_tool     = 0;

  ## Use a CSV parser for our read/write
  ## operations.
  my $csv =
    Text::CSV->new({ binary => 1, sep_char => ',', always_quote => 1 })
      or die "Cannot use CSV: ".Text::CSV->error_diag();

  #####################################
  ## Prepare the input file.
  open(my $infile, "<$f_input_events")
    or die "Couldn't read the course file: $f_input_events\n";

  #####################################
  ## Prepare the output file.
  open
    my $outfile,
    ">:encoding(utf8)",
    $f_output_events
      or die "$f_output_events: $!";

  $csv->print($outfile,
  [
    'event_day',
    'event_hour',
    'day_and_hour',
    'term_name',
    'course_canvas_id',
    'person_canvas_id',
    'csn',
    'tool_name',
    'is_link',
    'is_tool'
  ]);

  print $outfile "\n";

  #####################################
  ## Iterate over each raw event from
  ## the UDP Event store.
  while(my $line = <$infile>) {

    if( $csv->parse($line) ) {

      my @fields = $csv->fields();

      ##########################################################################
      ################ Extract raw event data ##################################
      ##########################################################################

      my $person_canvas_id = &value_or_undef($fields[0]);
      my $course_canvas_id = &value_or_undef($fields[1]);
      my $event_day        = &value_or_undef($fields[2]);
      my $event_hour       = &value_or_undef($fields[3]);
      my $tool_name        = &value_or_undef($fields[4]);
      my $tool_canvas_id   = &value_or_undef($fields[5]);
      my $tool_url         = &value_or_undef($fields[6]);

      next unless defined $person_canvas_id && $person_canvas_id ne '';
      next unless defined $course_canvas_id && $course_canvas_id ne '';

      ##########################################################################
      ################ Tool data ###############################################
      ##########################################################################

      ## Use the URL to generate a tool name. This is the result of
      ## mappings that are annoying to generate.
      my $tool =
        &value_or_undef(
          &tool_name(
            $udw_tools,
            $tool_name,
            $tool_url,
            $tool_canvas_id
          )
        );

      next if $tool->{is_tool} eq 'FALSE' && $config->{skip_links};

      $count_links++ if $tool->{is_link} eq 'TRUE';
      $count_tools++ if $tool->{is_tool} eq 'TRUE';

      $count_unknown_tool++ if ! $tool->{is_link} && ! $tool->{is_tool};

      ##########################################################################
      ################ Tool data ###############################################
      ##########################################################################
      $count_events++;

      ##########################################################################
      ################ User data ###############################################
      ##########################################################################

      ## Did we match a CS person?
      $count_match_cs_person++
        if defined $cs_persons->{$person_canvas_id};

      ##########################################################################
      ################ Course data #############################################
      ##########################################################################

      ## Grab other course identifiers, if they
      ## are available.
      my $course_ucdm_id =
        &value_or_undef($cs_course_offerings->{$course_canvas_id}->{course_ucdm_id});

      ## Did we match a CS course?
      $count_match_cs_course++
        if defined $cs_course_offerings->{$course_canvas_id};

      # Did we match a UDW course?
      $count_match_udw_course++
        if defined $udw_course_offerings->{$course_canvas_id};

      ## Did we fail to match a course?
      $count_match_no_course++
        if ! defined $cs_course_offerings->{$course_canvas_id} &&
           ! defined $udw_course_offerings->{$course_canvas_id};

      #################################################################
      ## Now we determine the rest of the enriched event data based
      ## on whether we have a Context store record corresponding to
      ## this event or not. We prefer that we do, since we have far
      ## richer information in the Context store with which to enrich
      ## our event data.
      my $term_name          = undef;
      my $csn                = undef;

      ## If we have a Context store record, use our Context store
      ## data.
      if( defined $course_ucdm_id && $course_ucdm_id ne '' ) {

        ## Term name.
        $term_name =
          &value_or_undef(
            $cs_course_offerings->{$course_canvas_id}->{term_name}
          );

        $term_name = ucfirst(lc(&clean($term_name)));

        $csn =
          $cs_course_offerings->{$course_canvas_id}->{course_subject} . ' ' .
          $cs_course_offerings->{$course_canvas_id}->{course_number};

      ## Otherwise, use UDW/Canvas data.
      } else {

        ## We need to use the UDW/Canvas Data code. Based on the
        ## institution, let's try to parse it and see what happens.
        my $course_code = $udw_course_offerings->{$course_canvas_id}->{course_code};

        ## Is this Indiana University?
        if( $course_code =~ m|(\w\w\d\d)-(\w+)-(\w+)-([a-zA-Z]*)(\d+)-(.*)| ) {

          my $course_subject = undef;
          my $course_number  = $5;

          if( $3 && $3 ne '' && $4 && $4 ne '' ) {
            $course_subject = "$3-$4";
          } else {
            $course_subject = $3;
          }

          $csn = "$course_subject $course_number";
        } else {
          $csn = $course_code;
        }

        ## Term name.
        $term_name =
          &value_or_undef($udw_course_offerings->{$course_canvas_id}->{term_name});
      }

      ##########################################################################
      ################ Time data ###############################################
      ##########################################################################

      ## Make day and hour for this event.
      my $day_and_hour = "$event_day $event_hour:00:00";

      ##########################################################################
      ################ Write event data ########################################
      ##########################################################################

      # Now we record the data from the event into the data structure
      # that becomes the source for data inputted into our temporal
      # by user, by course, by tool table.
      #
      # We only record this data if we have a course match or if
      # matches are not required.
      #
      $csv->print($outfile,
      [
        $event_day,
        $event_hour,
        $day_and_hour,
        $term_name,
        md5_hex($course_canvas_id),
        md5_hex($person_canvas_id),
        $csn,
        $tool->{tool_name},
        $tool->{is_link},
        $tool->{is_tool}
      ]);

      print $outfile "\n";

      ## Just a useful progress tracker in the CLI.
      print "Lines: $count_events\n" if $count_events % 1000 == 0;
    }
  }

  close($infile);

  ## Some information about the data we just processed.
  printf("Total events processed: %d.\n", $count_events);
  print "\n";
  printf("Total events matching CS course: %d (%.2f%%).\n",  $count_match_cs_course,  100*($count_match_cs_course/$count_events));
  print "\n";
  printf("Total events matching CS person: %d (%.2f%%).\n",  $count_match_cs_person,  100*($count_match_cs_person/$count_events));
  print "\n";
  printf("Links launched: %d (%.2f%%).\n", $count_links, 100*($count_links/$count_events));
  printf("Tools launched: %d (%.2f%%).\n", $count_tools, 100*($count_tools/$count_events));
  printf("Unknown tools launched: %d (%.2f%%).\n", $count_unknown_tool, 100*($count_unknown_tool/$count_events));

  return $events_output;
}

################################################################################
#################### Miscellaneous #############################################
################################################################################
sub fake_name($) {
  my($person_canvas_id) = @_;

  ## Die if we can't generate a fake name because of a bad value.
  die "Can't generate fake name for canvas ID '$person_canvas_id'.\n"
    if ! defined $person_canvas_id || $person_canvas_id eq '';

  ## Create a fake name for this Canvas ID
  ## if we don't already have one.
  unless( defined $fake_names->{$person_canvas_id} ) {
    $fake_names->{$person_canvas_id} = {
      first => $faker->first_name,
      last  => $faker->last_name
    };
  }

  return $fake_names->{$person_canvas_id};
}

sub value_or_undef($) {
  my($x) = @_;
  return $x if defined $x;
  return undef;
}

sub save_config {
  print "Saving config\n";
  open F, ">$f_conf" or die $!;
  print F Dump($config);
  close F;
}

sub clean($) {
  my($x) = @_;

  $x =~ s/s\+/ /g;
  $x =~ s/^\s+//;
  $x =~ s/\s+$//;

  return $x;
}

################################################################################
#################### Load UDW & CS data ########################################
################################################################################
sub load_cs_course_offerings($) {
  my($config) = @_;

  print "Loading course data from the UDP Context store.\n";

  ## A collection of the course offerings.
  my $cs_course_offerings = {};

  my $cs = &connect_to_database(
    $config->{context_store}->{name},
    $config->{context_store}->{host},
    $config->{context_store}->{port},
    $config->{context_store}->{user},
    $config->{context_store}->{pass}
  );

  my $query = <<"END_OF_SQL";
SELECT
  kmco.id course_ucdm_id
  , kmco.lms_ext_id course_canvas_id
  , at.name term_name
  , co.course_subj course_subject
  , co.course_no course_number
FROM keymap.course_offering kmco
INNER JOIN
  entity.course_offering co ON kmco.id=co.course_offering_id
INNER JOIN
  entity.academic_term at ON co.academic_term_id=at.academic_term_id
END_OF_SQL

  my $sth = $cs->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {
    my $course_canvas_id = $row->{course_canvas_id};

    $cs_course_offerings->{$course_canvas_id} =
      {
        course_ucdm_id     => $row->{course_ucdm_id},
        term_name          => $row->{term_name},
        course_subject     => $row->{course_subject},
        course_number      => $row->{course_number}
      };
  }

  &disconnect_from_database($cs);

  return $cs_course_offerings;
}

sub load_cs_persons($) {
  my($config) = @_;

  print "Loading person data from the UDP Context store.\n";

  ## A collection of the course offerings.
  my $cs_persons = {};

  my $cs = &connect_to_database(
    $config->{context_store}->{name},
    $config->{context_store}->{host},
    $config->{context_store}->{port},
    $config->{context_store}->{user},
    $config->{context_store}->{pass}
  );

  my $query = <<"END_OF_SQL";
SELECT
  kmp.id person_ucdm_id
  , kmp.lms_ext_id person_canvas_id
FROM
  entity.person p
INNER JOIN keymap.person kmp ON p.person_id=kmp.id
END_OF_SQL

  my $sth = $cs->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {
    my $person_canvas_id = $row->{person_canvas_id};

    ## Skip this record if there is no Canvas ID
    ## for the person.
    next unless defined $person_canvas_id && $person_canvas_id ne '';

    ## Get a fake first & last name for this
    ## person.
    my $fake_name = &fake_name($person_canvas_id);

    $cs_persons->{$person_canvas_id} =
      {
        person_ucdm_id => $row->{person_ucdm_id}
      };
  }

  &disconnect_from_database($cs);

  return $cs_persons;
}

sub load_udw_course_offerings($) {
  my($config) = @_;

  print "Loading course data from the UDW.\n";

  ## A collection of the course offerings.
  my $udw_course_offerings = {};

  my $udw = &connect_to_database(
    $config->{udw}->{name},
    $config->{udw}->{host},
    $config->{udw}->{port},
    $config->{udw}->{user},
    $config->{udw}->{pass}
  );

  my $query = <<"END_OF_SQL";
SELECT
  c.sis_source_id course_sis_id
  , c.id course_canvas_global_id
  , c.canvas_id course_canvas_id
  , c.code course_code
  , c.name course_name
  , count(cse.id) as course_enrollments
  , t.name term_name
FROM
  enrollment_dim cse
INNER JOIN course_section_dim cs ON cse.course_section_id=cs.id
INNER JOIN course_dim c ON cs.course_id=c.id
INNER JOIN enrollment_term_dim t ON c.enrollment_term_id=t.id
GROUP BY
  c.id,
  c.canvas_id,
  c.name,
  c.code,
  t.name,
  c.sis_source_id
;
END_OF_SQL

  my $sth = $udw->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {
    my $course_canvas_id = $row->{course_canvas_id};

    $udw_course_offerings->{$course_canvas_id} =
      {
        course_sis_id      => $row->{course_sis_id},

        term_name          => $row->{term_name},

        course_name        => $row->{course_name},
        course_code        => $row->{course_code},
        course_enrollments => $row->{course_enrollments}
      };
  }

  &disconnect_from_database($udw);

  return $udw_course_offerings;
}

#sub load_udw_persons($) {
#  my($config) = @_;

#  print "Loading person data from the UDW.\n";

  ## A collection of the persons.
#  my $udw_persons = {};

#  my $udw = &connect_to_database(
#    $config->{udw}->{name},
#    $config->{udw}->{host},
#    $config->{udw}->{port},
#    $config->{udw}->{user},
#    $config->{udw}->{pass}
#  );

#  my $query = <<"END_OF_SQL";
#SELECT
#  u.id
#  , u.canvas_id person_canvas_id
#  , p.sis_user_id sis_user_id
#FROM
#  pseudonym_dim p
#INNER JOIN
#  user_dim u ON p.user_id=u.id
#;
#END_OF_SQL

#  my $sth = $udw->prepare($query);
#     $sth->execute;

#  while(my $row = $sth->fetchrow_hashref) {
#    my $person_canvas_id = $row->{person_canvas_id};

    ## Get a fake first & last name for this
    ## person.
#    my $fake_name = &fake_name($person_canvas_id);

#    $udw_persons->{$person_canvas_id} =
#      {
#        sis_source_id => $row->{person_sis_id},

#        first => $fake_name->{first},
#        last  => $fake_name->{last}
#      };
#  }

#  &disconnect_from_database($udw);

#  return $udw_persons;
#}

sub load_udw_tools($) {
  my($config) = @_;

  print "Loading tools from the UDW.\n";

  ## A collection of the tools.
  my $udw_tools = {};

  my $udw = &connect_to_database(
    $config->{udw}->{name},
    $config->{udw}->{host},
    $config->{udw}->{port},
    $config->{udw}->{user},
    $config->{udw}->{pass}
  );

  my $query = <<"END_OF_SQL";
SELECT
  t.id tool_canvas_id
  , t.tool_id
  , t.url
  , t.name tool_name
  , t.description
FROM
  external_tool_activation_dim t
;
END_OF_SQL

  my $sth = $udw->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {
    my $tool_canvas_id = $row->{$row->{tool_canvas_id}};

    $udw_tools->{$tool_canvas_id} =
      {
        tool_id     => $row->{tool_id},
        url         => $row->{url},
        name        => $row->{tool_name},
        description => $row->{description}
      };
  }

  &disconnect_from_database($udw);

  return $udw_tools;
}

sub load_and_save_person_stats($$) {
  my($config, $file) = @_;

  print "Loading person stats from the UDP Context store (features).\n";

  ## Use a CSV parser for our read/write
  ## operations.
  my $csv =
    Text::CSV->new({ binary => 1, sep_char => ',', always_quote => 1 })
      or die "Cannot use CSV: ".Text::CSV->error_diag();

  #####################################
  ## Prepare the output file.
  open
    my $fh,
    ">:encoding(utf8)",
    $file
      or die "$file: $!";

#      'person_name',

  $csv->print($fh,
  [
    'person_canvas_id',
    'name',
    'sex',
    'is_sex_no_data',
    'is_sex_female',
    'is_sex_male',
    'is_sex_not_selected',
    'is_sex_none',
    'ethnicity',
    'is_ethnicity_no_data',
    'is_ethnicity_american_indian',
    'is_ethnicity_asian',
    'is_ethnicity_black',
    'is_ethnicity_hispanic_or_latino',
    'is_ethnicity_native_pacific_islander',
    'is_ethnicity_not_specified',
    'is_ethnicity_two_or_more_races',
    'is_ethnicity_white',
    'is_ethnicity_none',
    'gpa_hs',
    'gpa_college',
    'num_terms_completed',
    'num_course_completed',
    'sum_credits_taken',
    'sum_credits_earned',
    'count_course_sections_for_all_prev_term_gteq_0_lt_4',
    'count_course_sections_for_all_prev_term_gteq_4_lt_8',
    'count_course_sections_for_all_prev_term_gteq_8_lt_12',
    'count_course_sections_for_all_prev_term_gteq_12_lt_16',
    'count_course_sections_for_all_prev_term_gteq_16_lt_20',
    'count_course_sections_for_all_prev_term_gteq_20_lt_24',
    'count_course_sections_for_all_prev_term_gteq_24_lt_28',
    'count_course_sections_for_all_prev_term_gteq_28_lt_32',
    'count_course_sections_for_all_prev_term_gteq_32',
    'sum_credits_taken_for_all_prev_term_gteq_0_lt_15',
    'sum_credits_taken_for_all_prev_term_gteq_15_lt_30',
    'sum_credits_taken_for_all_prev_term_gteq_30_lt_45',
    'sum_credits_taken_for_all_prev_term_gteq_45_lt_60',
    'sum_credits_taken_for_all_prev_term_gteq_60_lt_75',
    'sum_credits_taken_for_all_prev_term_gteq_75_lt_90',
    'sum_credits_taken_for_all_prev_term_gteq_90_lt_105',
    'sum_credits_taken_for_all_prev_term_gteq_105_lt_120',
    'sum_credits_taken_for_all_prev_term_gteq_120',
    'sum_credits_earned_for_all_prev_term_gteq_0_lt_15',
    'sum_credits_earned_for_all_prev_term_gteq_15_lt_30',
    'sum_credits_earned_for_all_prev_term_gteq_30_lt_45',
    'sum_credits_earned_for_all_prev_term_gteq_45_lt_60',
    'sum_credits_earned_for_all_prev_term_gteq_60_lt_75',
    'sum_credits_earned_for_all_prev_term_gteq_75_lt_90',
    'sum_credits_earned_for_all_prev_term_gteq_90_lt_105',
    'sum_credits_earned_for_all_prev_term_gteq_105_lt_120',
    'sum_credits_earned_for_all_prev_term_gteq_120'
  ]);

  print $fh "\n";

  my $cs = &connect_to_database(
    $config->{context_store}->{name},
    $config->{context_store}->{host},
    $config->{context_store}->{port},
    $config->{context_store}->{user},
    $config->{context_store}->{pass}
  );

  my $query = <<"END_OF_SQL";
SELECT
  kmp.lms_ext_id person_canvas_id
  , p.sex sex
  , p.is_sex_no_data is_sex_no_data
  , p.is_sex_female is_sex_female
  , p.is_sex_male is_sex_male
  , p.is_sex_not_selected is_sex_not_selected
  , p.is_sex_none is_sex_none
  , p.ethnicity ethnicity
  , p.is_ethnicity_no_data is_ethnicity_no_data
  , p.is_ethnicity_american_indian is_ethnicity_american_indian
  , p.is_ethnicity_asian is_ethnicity_asian
  , p.is_ethnicity_black is_ethnicity_black
  , p.is_ethnicity_hispanic_or_latino is_ethnicity_hispanic_or_latino
  , p.is_ethnicity_native_pacific_islander is_ethnicity_native_pacific_islander
  , p.is_ethnicity_not_specified is_ethnicity_not_specified
  , p.is_ethnicity_two_or_more_races is_ethnicity_two_or_more_races
  , p.is_ethnicity_white is_ethnicity_white
  , p.is_ethnicity_none is_ethnicity_none
  , ep.hs_gpa gpa_hs
  , p.col_gpa_cum gpa_college
  , scbt.rank_person_academic_term_order_asc num_terms_completed
  , ep.course_count num_course_completed
  , scbt.sum_credits_taken sum_credits_taken
  , scbt.sum_credits_earned sum_credits_earned
  , scch.count_course_sections_for_all_prev_term_gteq_0_lt_4 count_course_sections_for_all_prev_term_gteq_0_lt_4
  , scch.count_course_sections_for_all_prev_term_gteq_4_lt_8 count_course_sections_for_all_prev_term_gteq_4_lt_8
  , scch.count_course_sections_for_all_prev_term_gteq_8_lt_12 count_course_sections_for_all_prev_term_gteq_8_lt_12
  , scch.count_course_sections_for_all_prev_term_gteq_12_lt_16 count_course_sections_for_all_prev_term_gteq_12_lt_16
  , scch.count_course_sections_for_all_prev_term_gteq_16_lt_20 count_course_sections_for_all_prev_term_gteq_16_lt_20
  , scch.count_course_sections_for_all_prev_term_gteq_20_lt_24 count_course_sections_for_all_prev_term_gteq_20_lt_24
  , scch.count_course_sections_for_all_prev_term_gteq_24_lt_28 count_course_sections_for_all_prev_term_gteq_24_lt_28
  , scch.count_course_sections_for_all_prev_term_gteq_28_lt_32 count_course_sections_for_all_prev_term_gteq_28_lt_32
  , scch.count_course_sections_for_all_prev_term_gteq_32 count_course_sections_for_all_prev_term_gteq_32
  , scch.sum_credits_taken_for_all_prev_term_gteq_0_lt_15 sum_credits_taken_for_all_prev_term_gteq_0_lt_15
  , scch.sum_credits_taken_for_all_prev_term_gteq_15_lt_30 sum_credits_taken_for_all_prev_term_gteq_15_lt_30
  , scch.sum_credits_taken_for_all_prev_term_gteq_30_lt_45 sum_credits_taken_for_all_prev_term_gteq_30_lt_45
  , scch.sum_credits_taken_for_all_prev_term_gteq_45_lt_60 sum_credits_taken_for_all_prev_term_gteq_45_lt_60
  , scch.sum_credits_taken_for_all_prev_term_gteq_60_lt_75 sum_credits_taken_for_all_prev_term_gteq_60_lt_75
  , scch.sum_credits_taken_for_all_prev_term_gteq_75_lt_90 sum_credits_taken_for_all_prev_term_gteq_75_lt_90
  , scch.sum_credits_taken_for_all_prev_term_gteq_90_lt_105 sum_credits_taken_for_all_prev_term_gteq_90_lt_105
  , scch.sum_credits_taken_for_all_prev_term_gteq_105_lt_120 sum_credits_taken_for_all_prev_term_gteq_105_lt_120
  , scch.sum_credits_taken_for_all_prev_term_gteq_120 sum_credits_taken_for_all_prev_term_gteq_120
  , scch.sum_credits_earned_for_all_prev_term_gteq_0_lt_15 sum_credits_earned_for_all_prev_term_gteq_0_lt_15
  , scch.sum_credits_earned_for_all_prev_term_gteq_15_lt_30 sum_credits_earned_for_all_prev_term_gteq_15_lt_30
  , scch.sum_credits_earned_for_all_prev_term_gteq_30_lt_45 sum_credits_earned_for_all_prev_term_gteq_30_lt_45
  , scch.sum_credits_earned_for_all_prev_term_gteq_45_lt_60 sum_credits_earned_for_all_prev_term_gteq_45_lt_60
  , scch.sum_credits_earned_for_all_prev_term_gteq_60_lt_75 sum_credits_earned_for_all_prev_term_gteq_60_lt_75
  , scch.sum_credits_earned_for_all_prev_term_gteq_75_lt_90 sum_credits_earned_for_all_prev_term_gteq_75_lt_90
  , scch.sum_credits_earned_for_all_prev_term_gteq_90_lt_105 sum_credits_earned_for_all_prev_term_gteq_90_lt_105
  , scch.sum_credits_earned_for_all_prev_term_gteq_105_lt_120 sum_credits_earned_for_all_prev_term_gteq_105_lt_120
  , scch.sum_credits_earned_for_all_prev_term_gteq_120 sum_credits_earned_for_all_prev_term_gteq_120
FROM
  features.student_courseload_by_term scbt
LEFT JOIN
  features.person p ON scbt.person_id=p.person_id
INNER JOIN
  keymap.person kmp ON p.person_id=kmp.id
INNER JOIN
  entity.person ep ON p.person_id=ep.person_id
LEFT JOIN
  features.academic_term at ON scbt.academic_term_id=at.academic_term_id
LEFT JOIN
  features.student_course_credit_history scch ON
    (
      scbt.person_id=scch.person_id
      AND scbt.academic_term_id=scch.academic_term_id
    )
WHERE
  scbt.academic_term_id=8
END_OF_SQL

  my $sth = $cs->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $person_canvas_id = $row->{person_canvas_id};

    ## Skip this record if there is no Canvas ID
    ## for the person.
    next unless defined $person_canvas_id && $person_canvas_id ne '';

    my $fake_name = &fake_name($person_canvas_id);

    my $person_name =
      $fake_name->{last} . ', ' . $fake_name->{first};

    my $d = [
      md5_hex($row->{person_canvas_id}),
      $person_name,
      $row->{sex},
      $row->{is_sex_no_data},
      $row->{is_sex_female},
      $row->{is_sex_male},
      $row->{is_sex_not_selected},
      $row->{is_sex_none},
      $row->{ethnicity},
      $row->{is_ethnicity_no_data},
      $row->{is_ethnicity_american_indian},
      $row->{is_ethnicity_asian},
      $row->{is_ethnicity_black},
      $row->{is_ethnicity_hispanic_or_latino},
      $row->{is_ethnicity_native_pacific_islander},
      $row->{is_ethnicity_not_specified},
      $row->{is_ethnicity_two_or_more_races},
      $row->{is_ethnicity_white},
      $row->{is_ethnicity_none},
      $row->{gpa_hs},
      $row->{gpa_college},
      $row->{num_terms_completed},
      $row->{num_course_completed},
      $row->{sum_credits_taken},
      $row->{sum_credits_earned},
      $row->{count_course_sections_for_all_prev_term_gteq_0_lt_4},
      $row->{count_course_sections_for_all_prev_term_gteq_4_lt_8},
      $row->{count_course_sections_for_all_prev_term_gteq_8_lt_12},
      $row->{count_course_sections_for_all_prev_term_gteq_12_lt_16},
      $row->{count_course_sections_for_all_prev_term_gteq_16_lt_20},
      $row->{count_course_sections_for_all_prev_term_gteq_20_lt_24},
      $row->{count_course_sections_for_all_prev_term_gteq_24_lt_28},
      $row->{count_course_sections_for_all_prev_term_gteq_28_lt_32},
      $row->{count_course_sections_for_all_prev_term_gteq_32},
      $row->{sum_credits_taken_for_all_prev_term_gteq_0_lt_15},
      $row->{sum_credits_taken_for_all_prev_term_gteq_15_lt_30},
      $row->{sum_credits_taken_for_all_prev_term_gteq_30_lt_45},
      $row->{sum_credits_taken_for_all_prev_term_gteq_45_lt_60},
      $row->{sum_credits_taken_for_all_prev_term_gteq_60_lt_75},
      $row->{sum_credits_taken_for_all_prev_term_gteq_75_lt_90},
      $row->{sum_credits_taken_for_all_prev_term_gteq_90_lt_105},
      $row->{sum_credits_taken_for_all_prev_term_gteq_105_lt_120},
      $row->{sum_credits_taken_for_all_prev_term_gteq_120},
      $row->{sum_credits_earned_for_all_prev_term_gteq_0_lt_15},
      $row->{sum_credits_earned_for_all_prev_term_gteq_15_lt_30},
      $row->{sum_credits_earned_for_all_prev_term_gteq_30_lt_45},
      $row->{sum_credits_earned_for_all_prev_term_gteq_45_lt_60},
      $row->{sum_credits_earned_for_all_prev_term_gteq_60_lt_75},
      $row->{sum_credits_earned_for_all_prev_term_gteq_75_lt_90},
      $row->{sum_credits_earned_for_all_prev_term_gteq_90_lt_105},
      $row->{sum_credits_earned_for_all_prev_term_gteq_105_lt_120},
      $row->{sum_credits_earned_for_all_prev_term_gteq_120},
    ];

    $csv->print($fh, $d);
    print $fh "\n";
  }

  &disconnect_from_database($cs);

  close $fh;

  return 1;
}

sub load_and_save_course_stats($$$) {
  my($config, $file, $udw_course_offerings) = @_;
  my %output_data = ();

  print "Loading person stats from the UDP Context store (features).\n";

  my $cs = &connect_to_database(
    $config->{context_store}->{name},
    $config->{context_store}->{host},
    $config->{context_store}->{port},
    $config->{context_store}->{user},
    $config->{context_store}->{pass}
  );

  my $query = <<"END_OF_SQL";
WITH course_discussions AS (
  SELECT
    COUNT(discussion_id) count_discussions
    , course_offering_id
    FROM entity.discussion d
    WHERE
      status = 'active'
    GROUP BY
      course_offering_id
    ORDER BY
      count_discussions DESC
)
, course_learning_activities AS (
  SELECT
    COUNT(learner_activity_id) count_learning_activities
    , course_offering_id
    FROM entity.learner_activity la
    WHERE
      status = 'published'
      AND visibility = 'everyone'
    GROUP BY
      course_offering_id
    ORDER BY
      count_learning_activities DESC
)
, course_modules AS (
  SELECT
    COUNT(module_id) count_modules
    , course_offering_id
    FROM entity.module m
    GROUP BY
      course_offering_id
    ORDER BY
      count_modules DESC
)
SELECT
  kmco.lms_ext_id course_canvas_id
  , at.session_name term_name
  , co.course_subj course_subject
  , co.course_no course_number
  , co.title course_name
  , sum(cssm.count_is_role_student) count_students
  , CASE
    WHEN cd.count_discussions IS NOT NULL
    THEN cd.count_discussions
    ELSE 0
    END count_discussions
  , CASE
    WHEN cla.count_learning_activities IS NOT NULL
    THEN cla.count_learning_activities
    ELSE 0
    END count_learning_activities
  , CASE
    WHEN cm.count_modules IS NOT NULL
    THEN cm.count_modules
    ELSE 0
    END count_modules
  , sum(cssm.count_is_sex_no_data) count_is_sex_no_data
  , sum(cssm.count_is_sex_no_data) / sum(cssm.count_is_role_student) perc_is_sex_no_data
  , sum(cssm.count_is_sex_female) count_is_sex_female
  , sum(cssm.count_is_sex_female) / sum(cssm.count_is_role_student) perc_is_sex_female
  , sum(cssm.count_is_sex_male) count_is_sex_male
  , sum(cssm.count_is_sex_male) / sum(cssm.count_is_role_student) perc_is_sex_male
  , sum(cssm.count_is_sex_none) count_is_sex_none
  , sum(cssm.count_is_sex_none) / sum(cssm.count_is_role_student) perc_is_sex_none
  , sum(cssm.count_is_sex_not_selected) count_is_sex_not_selected
  , sum(cssm.count_is_sex_not_selected) / sum(cssm.count_is_role_student) perc_is_sex_not_selected
  , sum(cssm.count_is_ethnicity_no_data) count_is_ethnicity_no_data
  , sum(cssm.count_is_ethnicity_no_data) / sum(cssm.count_is_role_student) perc_is_ethnicity_no_data
  , sum(cssm.count_is_ethnicity_american_indian) count_is_ethnicity_american_indian
  , sum(cssm.count_is_ethnicity_american_indian) / sum(cssm.count_is_role_student) perc_is_ethnicity_american_indian
  , sum(cssm.count_is_ethnicity_asian) count_is_ethnicity_asian
  , sum(cssm.count_is_ethnicity_asian) / sum(cssm.count_is_role_student) perc_is_ethnicity_asian
  , sum(cssm.count_is_ethnicity_black) count_is_ethnicity_black
  , sum(cssm.count_is_ethnicity_black) / sum(cssm.count_is_role_student) perc_is_ethnicity_black
  , sum(cssm.count_is_ethnicity_hispanic_or_latino) count_is_ethnicity_hispanic_or_latino
  , sum(cssm.count_is_ethnicity_hispanic_or_latino) / sum(cssm.count_is_role_student) perc_is_ethnicity_hispanic_or_latino
  , sum(cssm.count_is_ethnicity_native_pacific_islander) count_is_ethnicity_native_pacific_islander
  , sum(cssm.count_is_ethnicity_native_pacific_islander) / sum(cssm.count_is_role_student) perc_is_ethnicity_native_pacific_islander
  , sum(cssm.count_is_ethnicity_none) count_is_ethnicity_none
  , sum(cssm.count_is_ethnicity_none) / sum(cssm.count_is_role_student) perc_is_ethnicity_none
  , sum(cssm.count_is_ethnicity_not_specified) count_is_ethnicity_not_specified
  , sum(cssm.count_is_ethnicity_not_specified) / sum(cssm.count_is_role_student) perc_is_ethnicity_not_specified
  , sum(cssm.count_is_ethnicity_two_or_more_races) count_is_ethnicity_two_or_more_races
  , sum(cssm.count_is_ethnicity_two_or_more_races) / sum(cssm.count_is_role_student) perc_is_ethnicity_two_or_more_races
  , sum(cssm.count_is_ethnicity_white) count_is_ethnicity_white
  , sum(cssm.count_is_ethnicity_white) / sum(cssm.count_is_role_student) perc_is_ethnicity_white
FROM
  features.course_section_student_metrics cssm
INNER JOIN
  keymap.course_offering kmco ON cssm.course_offering_id=kmco.id
INNER JOIN
  entity.course_offering co ON kmco.id=co.course_offering_id
INNER JOIN
  entity.academic_term at ON co.academic_term_id=at.academic_term_id
LEFT JOIN course_discussions cd ON co.course_offering_id=cd.course_offering_id
LEFT JOIN course_learning_activities cla ON co.course_offering_id=cla.course_offering_id
LEFT JOIN course_modules cm ON co.course_offering_id=cm.course_offering_id
WHERE
  at.academic_term_id=8
GROUP BY
  course_canvas_id
  , at.session_name
  , co.course_subj
  , co.course_no
  , co.title
  , cd.count_discussions
  , cla.count_learning_activities
  , cm.count_modules
END_OF_SQL

  my $sth = $cs->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    ## Course ID
    my $course_canvas_id = $row->{course_canvas_id};

    ## Get the CSN.
    my $csn = $row->{course_subject} . ' ' . $row->{course_number};

    $output_data{$csn} = [
      $row->{term_name},
      md5_hex($course_canvas_id),
      $row->{course_subject},
      $row->{course_number},
      $csn,
      $row->{course_name},
      $row->{count_students},
      $row->{count_discussions},
      $row->{count_learning_activities},
      $row->{count_modules},
      $row->{count_is_sex_no_data},
      $row->{perc_is_sex_no_data},
      $row->{count_is_sex_female},
      $row->{perc_is_sex_female},
      $row->{count_is_sex_male},
      $row->{perc_is_sex_male},
      $row->{count_is_sex_none},
      $row->{perc_is_sex_none},
      $row->{count_is_sex_not_selected},
      $row->{perc_is_sex_not_selected},
      $row->{count_is_ethnicity_no_data},
      $row->{perc_is_ethnicity_no_data},
      $row->{count_is_ethnicity_american_indian},
      $row->{perc_is_ethnicity_american_indian},
      $row->{count_is_ethnicity_asian},
      $row->{perc_is_ethnicity_asian},
      $row->{count_is_ethnicity_black},
      $row->{perc_is_ethnicity_black},
      $row->{count_is_ethnicity_hispanic_or_latino},
      $row->{perc_is_ethnicity_hispanic_or_latino},
      $row->{count_is_ethnicity_native_pacific_islander},
      $row->{perc_is_ethnicity_native_pacific_islander},
      $row->{count_is_ethnicity_none},
      $row->{perc_is_ethnicity_none},
      $row->{count_is_ethnicity_not_specified},
      $row->{perc_is_ethnicity_not_specified},
      $row->{count_is_ethnicity_two_or_more_races},
      $row->{perc_is_ethnicity_two_or_more_races},
      $row->{count_is_ethnicity_white},
      $row->{perc_is_ethnicity_white}
    ];
  }

  &disconnect_from_database($cs);

  ##############################################################################
  ####################### Prepare the output file. #############################

  ## Read from the UDW courses.
  foreach my $course_canvas_id (keys $udw_course_offerings) {

    ## Skip if the data is from the wrong term.
    next unless $udw_course_offerings->{$course_canvas_id}->{term_name} eq 'FALL 2019';

    ## We need to use the UDW/Canvas Data code. Based on the
    ## institution, let's try to parse it and see what happens.
    my $course_code = $udw_course_offerings->{$course_canvas_id}->{course_code};

    ## Is this Indiana University?
    if( $course_code =~ m|(\w\w\d\d)-(\w+)-(\w+)-([a-zA-Z]*)(\d+)-(.*)| ) {

      my $course_subject = undef;
      my $course_number  = $5;

      if( $3 && $3 ne '' && $4 && $4 ne '' ) {
        $course_subject = "$3-$4";
      } else {
        $course_subject = $3;
      }

      my $csn = "$course_subject $course_number";

      if( ! defined $output_data{$csn} ) {
        $output_data{$csn} = [
          'Fall 2019',
          md5_hex($course_canvas_id),
          $course_subject,
          $course_number,
          $csn,
          $udw_course_offerings->{$course_canvas_id}->{course_name},
          $udw_course_offerings->{$course_canvas_id}->{course_enrollments},
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef,
          undef
        ];

      } else {
        $output_data{$course_canvas_id}[6] +=
          $udw_course_offerings->{$course_canvas_id}->{course_enrollments};
      }

    } else {
      print "Could not parse: $course_code\n";
    }
  }

  ##############################################################################
  ####################### Prepare the output file. #############################
  ##
  ## Use a CSV parser for our read/write
  ## operations.
  my $csv =
    Text::CSV->new({ binary => 1, sep_char => ',', always_quote => 1 })
      or die "Cannot use CSV: ".Text::CSV->error_diag();

  open
    my $fh,
    ">:encoding(utf8)",
    $file
      or die "$file: $!";

  $csv->print($fh,
    [
      'term_name',
      'course_canvas_id',
      'course_subject',
      'course_number',
      'csn',
      'course_name',
      'count_students',
      'count_discussions',
      'count_learning_activities',
      'count_modules',
      'count_is_sex_no_data',
      'perc_is_sex_no_data',
      'count_is_sex_female',
      'perc_is_sex_female',
      'count_is_sex_male',
      'perc_is_sex_male',
      'count_is_sex_none',
      'perc_is_sex_none',
      'count_is_sex_not_selected',
      'perc_is_sex_not_selected',
      'count_is_ethnicity_no_data',
      'perc_is_ethnicity_no_data',
      'count_is_ethnicity_american_indian',
      'perc_is_ethnicity_american_indian',
      'count_is_ethnicity_asian',
      'perc_is_ethnicity_asian',
      'count_is_ethnicity_black',
      'perc_is_ethnicity_black',
      'count_is_ethnicity_hispanic_or_latino',
      'perc_is_ethnicity_hispanic_or_latino',
      'count_is_ethnicity_native_pacific_islander',
      'perc_is_ethnicity_native_pacific_islander',
      'count_is_ethnicity_none',
      'perc_is_ethnicity_none',
      'count_is_ethnicity_not_specified',
      'perc_is_ethnicity_not_specified',
      'count_is_ethnicity_two_or_more_races',
      'perc_is_ethnicity_two_or_more_races',
      'count_is_ethnicity_white',
      'perc_is_ethnicity_white'
    ]
  );

  print $fh "\n";

  foreach my $key (keys %output_data) {
    $csv->print($fh, $output_data{$key});
    print $fh "\n";
  }

  close $fh;

  return 1;
}

sub load_and_save_person_course_stats($$) {
  my($config, $file) = @_;

  print "Loading person stats from the UDP Context store (features).\n";

  ## Use a CSV parser for our read/write
  ## operations.
  my $csv =
    Text::CSV->new({ binary => 1, sep_char => ',', always_quote => 1 })
      or die "Cannot use CSV: ".Text::CSV->error_diag();

  #####################################
  ## Prepare the output file.
  open
    my $fh,
    ">:encoding(utf8)",
    $file
      or die "$file: $!";

  $csv->print($fh,
  [
    'term_name',
    'course_canvas_id',
    'person_canvas_id',
    'course_subject',
    'course_number',
    'csn',
    'current_score',
    'final_score'
  ]);

  print $fh "\n";

  my $cs = &connect_to_database(
    $config->{context_store}->{name},
    $config->{context_store}->{host},
    $config->{context_store}->{port},
    $config->{context_store}->{user},
    $config->{context_store}->{pass}
  );

  my $query = <<"END_OF_SQL";
SELECT
  kmp.lms_ext_id person_canvas_id
  , kmco.lms_ext_id course_canvas_id
  , at.session_name term_name
  , co.course_subj course_subject
  , co.course_no course_number
  , cg.current_score current_score
  , cg.final_score final_score
FROM
  entity.course_grade cg
INNER JOIN
  entity.person p ON cg.person_id=p.person_id
INNER JOIN
  entity.course_section cs ON cg.course_section_id=cs.course_section_id
INNER JOIN
  entity.course_offering co ON cs.course_offering_id=co.course_offering_id
INNER JOIN
  entity.academic_term at ON co.academic_term_id=at.academic_term_id
INNER JOIN
  keymap.course_offering kmco ON co.course_offering_id=kmco.id
INNER JOIN
  keymap.person kmp ON p.person_id=kmp.id
WHERE
  at.academic_term_id=8
GROUP BY
  kmp.lms_ext_id,
  kmco.lms_ext_id,
  at.session_name,
  co.course_subj,
  co.course_no,
  cg.current_score,
  cg.final_score
END_OF_SQL

  my $sth = $cs->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $person_canvas_id = $row->{person_canvas_id};

    ## Skip this record if there is no Canvas ID
    ## for the person.
    next unless defined $person_canvas_id && $person_canvas_id ne '';

    ## Get the CSN.
    my $csn = $row->{course_subject} . ' ' . $row->{course_number};

    my $d = [
      $row->{term_name},
      md5_hex($row->{course_canvas_id}),
      md5_hex($row->{person_canvas_id}),
      $row->{course_subject},
      $row->{course_number},
      $csn,
      $row->{current_score},
      $row->{final_score},
    ];

    $csv->print($fh, $d);
    print $fh "\n";
  }

  &disconnect_from_database($cs);

  close $fh;

  return 1;
}

################################################################################
#################### Mappings functions ########################################
################################################################################
sub get_value_from_user($) {
  my($default) = @_;
  my $return = '';

  while($return eq '') {

    ## Prompt the user for a value, give them a
    ## default that they can set by pressing
    ## enter.
    print "[$default]> ";
    my $captured = <STDIN>;
    chomp $captured;

    ## Set our return value to what we captured
    ## if its value wasn't "quit" and wasn't an
    ## empty string.
    $return = $captured if( $captured ne 'quit' && $captured ne '' );

    ## Set our return value to the default value
    ## if it is an empty string.
    $return = $default if $captured eq '';

    last if $captured eq 'quit';
  }

  print "Returning: -$return-\n";
  return $return;
}

sub get_or_set_tool_name($$$$$$$) {
  my($type, $subtype, $tool, $name, $domain, $url, $tool_canvas_id) = @_;

  my $key = undef;

  ## Set the proper key for the mapping.
  if( $subtype eq 'name' ) {
    $key = $name;
  } elsif( $subtype eq 'url' ) {
    $key = $url;
  } elsif( $subtype eq 'tool_canvas_id' ) {
    $key = $name;
  } else {
    die "Invalid subtype used in get_or_set_tool_name ($subtype).\n";
  }

  ## What we do depends on whether this tool is a link
  ## or an actual, installed LTI tool. First, handle
  ## the case where we have a link.
  if( defined $config->{$type}->{$subtype}->{$key} ) {
    $tool->{tool_name} = $config->{$type}->{$subtype}->{$key};

  ## We don't have a published tool name based on our
  ## input tool name. So we're going to prompt the user
  ## to decide what this tool should be called.
  } elsif( ! defined $config->{$type}->{$subtype}->{$key} ) {

    ## prompt the user for a name for this tool.
    print "[$type/$subtype]; name: $name; domain: $domain; url: $url\n";

    my $new_name = &get_value_from_user($key);

    $tool->{tool_name} =
      $config->{$type}->{$subtype}->{$key} =
        $new_name;

    ## Save any updated mappings to our config file.
    &save_config;
  }

  return 1;
}

## We'll assume that these are Canvas Live Events.
## Given this, any tool whose domain is edu-apps.org
## is actually a link and not an LTI Tool launch. So
## we want to treat and name these differently.
sub tool_name($$$$) {
  my($udw_tools, $name, $url, $tool_canvas_id) = @_;

  ## Tool data – this is what we return to the
  ## event processer.
  my $tool = {
    is_link => undef,
    is_tool => undef,
    name    => undef
  };

  ## Clean up the given name.
  $name = &clean($name);

  ## Clean up the given domain.
  my $domain = $url;
     $domain =~ s!^https?://(?:www\.)?!!i;
     $domain =~ s!/.*!!;
     $domain =~ s/[\?\#\:].*//;

  ## Clean up the tool id
  ##
  my $tool_id = $udw_tools->{$tool_canvas_id};

  #############################################################################
  ## Now we need to determine if we have a tool
  ## or a link. This is not as straightforward
  ## as it might seem with Canvas Live Events and
  ## Canvas data.
  ##
  ## We use TRUE and FALSE as values because BQ
  ## will auto-detect the data type as boolean.
  ##
  ## 1. If we have a tool_canvas_id, the tool_id
  ##    value is 'redirect', and we have a domain
  ##    value of 'edu-apps.org', then we have a
  ##    link.
  if( $tool_canvas_id && $tool_canvas_id ne '' && ($tool_id eq 'redirect' || $tool_id eq '') && $domain eq 'edu-apps.org' ) {
   $tool->{is_link} = 'TRUE';
   $tool->{is_tool} = 'FALSE';
  }

  ## 2. Then we have a number of conditions that we don't think
  ##    can be held. Check them all here.
  elsif( $tool_canvas_id && $tool_canvas_id ne '' && $tool_id eq 'redirect' && $domain ne 'edu-apps.org' ) {
   die
    'Invalid condition, tool_id is redirect but domain is not edu-apps.org ',
    "(name: '$name'; domain: '$domain', url: '$url', tool_canvas_id: $tool_canvas_id; tool_id: $tool_id).",
    "\n";
  }

  elsif( $tool_canvas_id && $tool_canvas_id ne '' && $tool_id ne 'redirect' && $domain eq 'edu-apps.org' ) {
   die
    'Invalid condition, tool_id is not redirect but domain is edu-apps.org ',
    "(name: '$name'; domain: '$domain', url: '$url', tool_canvas_id: $tool_canvas_id; tool_id: $tool_id).",
    "\n";
  }

  ## 4. We have a tool id and it's a tool!
  elsif( $tool_canvas_id && $tool_canvas_id ne '' && $tool_id ne 'redirect' && $domain ne 'edu-apps.org' ) {
   $tool->{is_link} = 'FALSE';
   $tool->{is_tool} = 'TRUE';
  }

  ## 3. We can't rely on the tool_canvas_id or tool_id, so let's
  ##    use the domain only.
  elsif( (! $tool_canvas_id || $tool_canvas_id eq '') && $domain eq 'edu-apps.org' ) {
   $tool->{is_link} = 'TRUE';
   $tool->{is_tool} = 'FALSE';
  }

  elsif( (! $tool_canvas_id || $tool_canvas_id eq '') && $domain ne 'edu-apps.org' && $domain ne '' ) {
   $tool->{is_link} = 'FALSE';
   $tool->{is_tool} = 'TRUE';
  }

  ## 4. This condition is achieved if there is no
  ##    tool_id and no domain value. We simply
  ##    throw a warning.
  elsif( $domain eq '' ) {

   warn
    'Could not determine if we have a tool or link ',
    "(name: '$name'; domain: '$domain', url: '$url', tool_canvas_id: $tool_canvas_id; tool_id: $tool_id).",
    "\n";

   $tool->{is_link} = $tool->{is_tool} = undef;

  } else {
   die
    'Something awful happened ',
    "(name: '$name'; domain: '$domain', url: '$url', tool_canvas_id: $tool_canvas_id; tool_id: $tool_id).\n";
  }

  #############################################################################
  ##
  ## Now that we know if this is a link  or a tool, we will
  ## determine the right mapping to a common tool name.
  ##
  ## We prefer the url as the primary mapping. If that fails,
  ## we rely on the name and then, as a final backstop, on
  ## the canvas-configured tool name.
  ##
  if( $tool->{is_link} eq 'TRUE' ) {

    ## If we have an undef or blank value for this
    ## link name, then we give it "Unknown.""
    $tool->{tool_name} = 'Unknown' if $name eq '';

    ## If we auto-set link names, then set the name
    ## to the given name from the event.
    $tool->{tool_name} = $name
      if $config->{auto_set_links} && ! $tool->{tool_name};

    ## If we're not auto-setting and we don't have a
    ## name value, then get the tool name from our
    ## existing mappings or prompt the user to set it.
    &get_or_set_tool_name('link', 'url', $tool, $name, $domain, $url, $tool_canvas_id)
      if ! $tool->{tool_name};

  ## If we have a tool, then we progress through a series
  ## of ways to set the value of this tool name.
  } elsif( $tool->{is_tool} eq 'TRUE' ) {

    ## This depends on whether we have a name, url, or
    ## tool_id available for us.
    if( $url && $url ne '' ) {
      &get_or_set_tool_name('tool', 'url', $tool, $name, $domain, $url)
        if $tool->{is_tool} eq 'TRUE';

    } elsif( $name && $name ne '' ) {
      &get_or_set_tool_name('tool', 'name', $tool, $name, $domain, $url, $tool_canvas_id)
        if $tool->{is_tool} eq 'TRUE';

    } elsif( $tool_canvas_id && $tool_canvas_id ne '' ) {

      ## Use the name of the tool from the UDW
      ## if there is a valid value available.
      $name = $udw_tools->{$tool_canvas_id}->{tool_name}
        if $udw_tools->{$tool_canvas_id}->{tool_name}
           && $udw_tools->{$tool_canvas_id}->{tool_name} ne '';

      &get_or_set_tool_name('tool', 'tool_canvas_id', $tool, $name, $domain, $url, $tool_canvas_id)
        if $tool->{is_tool} eq 'TRUE';

    } else {
      die "We have nothing to match this tool.\n";

    }
  }

  return $tool;
}

################################################################################
#################### SQL Functions #############################################
################################################################################
sub connect_to_database {
  my( $cs_name, $cs_host, $cs_port, $cs_user, $cs_pass ) = @_;

  my $cacs_dbh = DBI->connect("dbi:Pg:dbname=$cs_name;host=$cs_host;port=$cs_port", $cs_user, $cs_pass)
    || die "Could not connect to database\n";

  $cacs_dbh->{AutoCommit} = 0;
  $cacs_dbh->{RaiseError} = 1;
  $cacs_dbh->do("SET TIME ZONE 'UTC'");

  return $cacs_dbh;
}

sub disconnect_from_database {
  my($cacs_dbh) = @_;

  $cacs_dbh->disconnect;

  return 1;
}
